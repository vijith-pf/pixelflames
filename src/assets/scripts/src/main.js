var commonInit = {
  initCursor: function() {
      var cursor = $(".cursor"),
          follower = $(".cursor-follower");

      var posX = 0,
          posY = 0;

      var mouseX = 0,
          mouseY = 0;

      TweenMax.to({}, 0.016, {
          repeat: -1,
          onRepeat: function() {
              posX += (mouseX - posX) / 6;
              posY += (mouseY - posY) / 6;

              TweenMax.set(follower, {
                  css: {
                      left: posX - 6,
                      top: posY - 6
                  }
              });

              TweenMax.set(cursor, {
                  css: {
                      left: mouseX,
                      top: mouseY
                  }
              });
          }
      });

      $(document).on("mousemove", function(e) {
          mouseX = e.pageX;
          mouseY = e.pageY;
      });

      $(".burger-menu, a, .bg-holder-wrap").on("mouseenter", function() {
          cursor.addClass("active");
          follower.addClass("active");
      });
      $(".burger-menu, a, .bg-holder-wrap").on("mouseleave", function() {
          cursor.removeClass("active");
          follower.removeClass("active");
      });
  },
  initTilt: function() {
    $('.bg-holder-inner-wrap').tilt({
      maxTilt: 20,
      perspective: 1000, // Transform perspective, the lower the more extreme the tilt gets.
      easing: "cubic-bezier(.03,.98,.52,.99)", // Easing on enter/exit.
      scale: 1, // 2 = 200%, 1.5 = 150%, etc..
      speed: 300, // Speed of the enter/exit transition.
      transition: true, // Set a transition on enter/exit.
      disableAxis: null, // What axis should be disabled. Can be X or Y.
      reset: true, // If the tilt effect has to be reset on exit.
      glare: false, // Enables glare effect
      maxGlare: 1
    });
  },
  showPreloader: function() {
    $('.preloader-brand').removeClass('hide');
  },
  hidePreloader: function() {
    $('.preloader-brand').addClass('hide');
  },
  showComponents: function(){
    $('.brand, .burger-menu, .site-icon').addClass('show');
  },
  hideComponents: function(){
    $('.brand, .burger-menu, .site-icon').removeClass('show');
  },
  startContactAnimate: function() {
    $('#contactSection .linkAnimate1').addClass('startAnim');
    $('#contactSection .linkAnimate2').addClass('startAnim');

    var captionAnimate1 = new RevealFx(document.querySelector('#contactSection .captionAnimate1'), {
      revealSettings : {
        bgcolor: '#f6323e',
        onCover: function(contentEl, revealerEl) {
          contentEl.style.opacity = 1;
        }
      }
    });

    var captionAnimate2 = new RevealFx(document.querySelector('#contactSection .captionAnimate2'), {
      revealSettings : {
        bgcolor: '#ff9e14',
        delay: 250,
        onCover: function(contentEl, revealerEl) {
          contentEl.style.opacity = 1;
        }
      }
    });
      
    
    setTimeout(() => {
      captionAnimate1.reveal();
      captionAnimate2.reveal();
    }, 500);

  },
  endContactAnimate: function() {
    $('#contactSection .linkAnimate1').removeClass('startAnim');
    $('#contactSection .linkAnimate2').removeClass('startAnim');
  },
  animateBg: function(index, nextIndex) {
    var bgAnimation = new TimelineLite();
    $('.overlay-item').removeClass('animateBg');
    bgAnimation.staggerTo('#sectionCount'+nextIndex+' .overlay-item', 0.3, { css: { className: '+=animateBg' } }, 0.1);
  }
}

var settings = {
    overlayWrap: '.overlay-wrap',
    overlayElems: '.overlay-item',
    heroBanner: '.hero-banner-block',
    overlayWrapPrefix: '.overlay-wrap-',
    overlayElamPrefix: '.overlay-panel-',
    bannerPrefix: '.hero-banner-block-'
};

var initHomepageAnimations = {
    initFullpage: function() {
        $('#homePageFullpage').fullpage({
            //Navigation
            //menu: '#menu',
            lockAnchors: false,
            navigation: false,
            navigationPosition: 'right',
            showActiveTooltip: false,
            slidesNavigation: false,
            slidesNavPosition: 'bottom',

            //Scrolling
            css3: false,
            scrollingSpeed: 600,
            autoScrolling: true,
            fitToSection: false,
            fitToSectionDelay: 1500,
            scrollBar: false,
            //easing: 'easeInOutCubic',
            //easingcss3: 'ease',
            loopBottom: false,
            loopTop: false,
            loopHorizontal: true,
            continuousVertical: false,
            continuousHorizontal: false,
            scrollHorizontally: false,
            interlockedSlides: false,
            dragAndMove: false,
            offsetSections: false,
            resetSliders: false,
            fadingEffect: false,
            // normalScrollElements: '#normalScroll',
            scrollOverflow: true,
            scrollOverflowReset: false,
            scrollOverflowOptions: {
                scrollbars: false,
                mouseWheel: true,
                hideScrollbars: false,
                fadeScrollbars: false,
                disableMouse: true,
                interactiveScrollbars: true
            },
            touchSensitivity: 15,
            bigSectionsDestination: null,

            //Accessibility
            keyboardScrolling: true,
            animateAnchor: true,
            recordHistory: true,

            //Design
            controlArrows: true,
            verticalCentered: true,
            //sectionsColor: ['#ccc', '#fff'],
            paddingTop: '0px',
            paddingBottom: '0px',
            //fixedElements: '#header, .footer',
            responsiveWidth: 0,
            responsiveHeight: 0,
            responsiveSlides: false,
            parallax: false,
            parallaxOptions: { type: 'reveal', percentage: 62, property: 'translate' },
            cards: false,
            cardsOptions: { perspective: 100, fadeContent: true, fadeBackground: true },

            //Custom selectors
            sectionSelector: '.slide-section',
            lazyLoading: true,

            //events
            onLeave: function(origin, destination, direction) {
                
                initHomepageAnimations.showRevealOverlay();  
                commonInit.showPreloader();
                commonInit.hideComponents();
                
                initHomepageAnimations.removeDefaultOverlay();

                if(destination == 4){
                  setTimeout(() => {
                    initHomepageAnimations.startWelcomeReveal();
                  }, 1800);
                } else {
                  $('.reveal-wrap').css({ 'opacity': 0 });
                }

            },
            afterLoad: function(origin = 0, destination, direction) {
                initHomepageAnimations.projectBgAnim(origin, destination);
                setTimeout(function() {
                    initHomepageAnimations.projectInfoAnim(destination - 1, destination);
                }, 800);
                /*
                $('.normal-scroll-section .fp-scrollable').slimScroll({
                    alwaysVisible: true,
                    color: 'black',
                    size: '10px',
                    allowPageScroll: true,
                }).bind('slimscrolling', function (e, pos) {
                  console.log(pos);
                });
                */
            },
            afterRender: function() {},
            afterResize: function(width, height) {},
            afterReBuild: function() {},
            afterResponsive: function(isResponsive) {},
            afterSlideLoad: function(section, origin, destination, direction) {},
            onSlideLeave: function(section, origin, destination, direction) {}
        });
    },
    randomNumber: function(min, max) {
        //return Math.floor(Math.random() * (1 + max - min) + min);
        return Math.random() * (max - min) + min;
    },
    projectInfoAnim: function(origin, destination) {
        $(settings.heroBanner).removeClass('show-popup');
        $(settings.bannerPrefix + destination).addClass('show-popup');
    },
    addDefaultOverlay: function(origin, destination) {

      var skewModelDestination = $(settings.overlayWrapPrefix + destination + '.overlay-default ' + settings.overlayElems).toArray();
      skewModelDestination.sort(function() { return 0.5 - Math.random() });

      var tl1 = new TimelineMax();
      tl1.staggerTo(
          skewModelDestination, 3, {
              cycle: {
                  opacity: [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
              },
              repeat: -1,
              repeatDelay: 0.6,
              yoyo: true
          },
          3
      );
      setTimeout(() => {
          $(".overlay-default").addClass('show-default-shapes');
      }, 1000);
    },
    showRevealOverlay: function() {
        $('.overlay-reveal .overlay-item').removeClass('reveal-image');
    },
    removeDefaultOverlay: function() {
        $(".overlay-default").removeClass('show-default-shapes');
    },
    startWelcomeReveal: function() {
      $('.reveal-wrap').css({ 'opacity': 1 });
      var welcome1 = new RevealFx(document.querySelector('#welcome-1'), {
        revealSettings : {
          bgcolor: '#f6323e',
          onCover: function(contentEl, revealerEl) {
            contentEl.style.opacity = 1;
          }
        }
      });
      welcome1.reveal();

      var welcome2 = new RevealFx(document.querySelector('#welcome-2'), {
        revealSettings : {
          bgcolor: '#ff9e14',
          delay: 250,
          onCover: function(contentEl, revealerEl) {
            contentEl.style.opacity = 1;
          }
        }
      });
      welcome2.reveal();
    },
    projectBgAnim: function(origin, destination) {

        var tl = new TimelineLite();
        tl.staggerTo(settings.overlayWrapPrefix + destination + '.overlay-reveal ' + settings.overlayElems, .5, { css: { className: '+=reveal-image' } }, 0.2);
        
        setTimeout(() => {
          commonInit.hidePreloader();  
        }, 200);
        commonInit.showComponents();
        initHomepageAnimations.addDefaultOverlay(origin, destination);

    }
}

var initProjectListAnimations = {
  animateProjectsList: function(nextIndex, currentIndex, projectCount) {
    var projectListRowTl = new TimelineLite();
    $('.project-item').removeClass('fadeInUpProjects');
    if(nextIndex == currentIndex) {
      if(projectCount != undefined){
        var projectRow = projectCount;
      } else {
        var projectRow = currentIndex - 1;  
      }
      projectListRowTl.staggerTo('#projectListRow'+projectRow+ ' .project-item', 0.3, { css: { className: '+=fadeInUpProjects' } }, 0.3);
    }
  },
  initFullpage: function() {
    $('#projectListFullpage').fullpage({
      //Navigation
            //menu: '#menu',
            lockAnchors: false,
            navigation: false,
            navigationPosition: 'right',
            showActiveTooltip: false,
            slidesNavigation: false,
            slidesNavPosition: 'bottom',

            //Scrolling
            css3: false,
            scrollingSpeed: 600,
            autoScrolling: true,
            fitToSection: false,
            fitToSectionDelay: 1500,
            scrollBar: false,
            //easing: 'easeInOutCubic',
            //easingcss3: 'ease',
            loopBottom: false,
            loopTop: false,
            loopHorizontal: true,
            continuousVertical: false,
            continuousHorizontal: false,
            scrollHorizontally: false,
            interlockedSlides: false,
            dragAndMove: false,
            offsetSections: false,
            resetSliders: false,
            fadingEffect: false,
            // normalScrollElements: '#normalScroll',
            scrollOverflow: true,
            scrollOverflowReset: false,
            scrollOverflowOptions: {
                scrollbars: false,
                mouseWheel: true,
                hideScrollbars: false,
                fadeScrollbars: false,
                disableMouse: true,
                interactiveScrollbars: true
            },
            touchSensitivity: 15,
            bigSectionsDestination: null,

            //Accessibility
            keyboardScrolling: true,
            animateAnchor: true,
            recordHistory: true,

            //Design
            controlArrows: true,
            verticalCentered: true,
            //sectionsColor: ['#ccc', '#fff'],
            paddingTop: '0px',
            paddingBottom: '0px',
            //fixedElements: '#header, .footer',
            responsiveWidth: 0,
            responsiveHeight: 0,
            responsiveSlides: false,
            parallax: false,
            parallaxOptions: { type: 'reveal', percentage: 62, property: 'translate' },
            cards: false,
            cardsOptions: { perspective: 100, fadeContent: true, fadeBackground: true },

            //Custom selectors
            sectionSelector: '.slide-section',
            lazyLoading: true,

            onLeave: function(index, nextIndex, direction) {

              commonInit.hideComponents();

              initProjectListAnimations.animateProjectsList(nextIndex, 2);
              initProjectListAnimations.animateProjectsList(nextIndex, 3);
              initProjectListAnimations.animateProjectsList(nextIndex, 4);

              commonInit.endContactAnimate();
              if(nextIndex == 4) {
                commonInit.startContactAnimate();
              }

            },
            afterLoad: function(index, nextIndex) {
              commonInit.showComponents();

              var $projectDesc = $('.title h2.description');
              $projectDesc.removeClass('animations fadeInUp');
              $('#projectListCaptionWrap').css({ 'opacity': 0 });

              if( nextIndex == 1 ) {
                
                $('#projectListCaptionWrap').css({ 'opacity': 1 });
                var projectListCaption1 = new RevealFx(document.querySelector('#projectListCaption1'), {
                  revealSettings : {
                    bgcolor: '#fa5123',
                    onCover: function(contentEl, revealerEl) {
                      contentEl.style.opacity = 1;
                    }
                  }
                });
                projectListCaption1.reveal();

                var projectListCaption2 = new RevealFx(document.querySelector('#projectListCaption2'), {
                  revealSettings : {
                    bgcolor: '#ff9e14',
                    delay: 250,
                    onCover: function(contentEl, revealerEl) {
                      contentEl.style.opacity = 1;
                    }
                  }
                });
                projectListCaption2.reveal();

                $projectDesc.addClass('animations fadeInUp').css('animation-delay', '0.8s');

              }

              commonInit.animateBg(index, nextIndex);

              
            }

    });
  }
}

var initProjectDetailAnimations = {
  initFullpage: function() {
    $('#projectDetailFullpage').fullpage({
      //Navigation
            //menu: '#menu',
            lockAnchors: false,
            navigation: false,
            navigationPosition: 'right',
            showActiveTooltip: false,
            slidesNavigation: false,
            slidesNavPosition: 'bottom',

            //Scrolling
            css3: false,
            scrollingSpeed: 600,
            autoScrolling: true,
            fitToSection: false,
            fitToSectionDelay: 1500,
            scrollBar: false,
            //easing: 'easeInOutCubic',
            //easingcss3: 'ease',
            loopBottom: false,
            loopTop: false,
            loopHorizontal: true,
            continuousVertical: false,
            continuousHorizontal: false,
            scrollHorizontally: false,
            interlockedSlides: false,
            dragAndMove: false,
            offsetSections: false,
            resetSliders: false,
            fadingEffect: false,
            // normalScrollElements: '#normalScroll',
            scrollOverflow: true,
            scrollOverflowReset: false,
            scrollOverflowOptions: {
                scrollbars: false,
                mouseWheel: true,
                hideScrollbars: false,
                fadeScrollbars: false,
                disableMouse: true,
                interactiveScrollbars: true
            },
            touchSensitivity: 15,
            bigSectionsDestination: null,

            //Accessibility
            keyboardScrolling: true,
            animateAnchor: true,
            recordHistory: true,

            //Design
            controlArrows: true,
            verticalCentered: true,
            //sectionsColor: ['#ccc', '#fff'],
            paddingTop: '0px',
            paddingBottom: '0px',
            //fixedElements: '#header, .footer',
            responsiveWidth: 0,
            responsiveHeight: 0,
            responsiveSlides: false,
            parallax: false,
            parallaxOptions: { type: 'reveal', percentage: 62, property: 'translate' },
            cards: false,
            cardsOptions: { perspective: 100, fadeContent: true, fadeBackground: true },

            //Custom selectors
            sectionSelector: '.slide-section',
            lazyLoading: true,

            onLeave: function(index, nextIndex, direction) {

              commonInit.hideComponents();
              commonInit.endContactAnimate();

              var $projectImg = $('#sectionCount2 .screen-shot');
              $projectImg.removeClass('animations fadeInUp');
              if(nextIndex == 2) {
                $projectImg.addClass('animations fadeInUp').css('animation-delay', '0.8s');
              }

              var $projectTitle = $('#projectDetailFullpage .project-case-info .title h2');
              var $projectInfo1 = $('#projectDetailFullpage .project-case-info .projectInfo1');
              var $projectInfo2 = $('#projectDetailFullpage .project-case-info .projectInfo2');
              var $projectInfo3 = $('#projectDetailFullpage .project-case-info .projectInfo3');
              var $projectInfo4 = $('#projectDetailFullpage .project-case-info .projectInfo4');
              $projectTitle.removeClass('animations fadeInUp');
              $projectInfo1.removeClass('animations fadeInUp');
              $projectInfo2.removeClass('animations fadeInUp');
              $projectInfo3.removeClass('animations fadeInUp');
              $projectInfo4.removeClass('animations fadeInUp');
              if(nextIndex == 3) {
                $projectTitle.addClass('animations fadeInUp').css('animation-delay', '0.8s');
                $projectInfo1.addClass('animations fadeInUp').css('animation-delay', '1.2s');
                $projectInfo2.addClass('animations fadeInUp').css('animation-delay', '1.3s');
                $projectInfo3.addClass('animations fadeInUp').css('animation-delay', '1.5s');
                $projectInfo4.addClass('animations fadeInUp').css('animation-delay', '1.6s');
              }

              if(nextIndex == 4) {
                commonInit.startContactAnimate();
              }

            },
            afterLoad: function(index, nextIndex) {
              commonInit.showComponents();
              
              $('#projectDetailCaptionWrap').css({ 'opacity': 0 });
              $('#sectionCount1 .project-banner').removeClass('animateImg');

              if( nextIndex == 1 ) {

                $('#sectionCount1 .project-banner').addClass('animateImg');
                
                $('#projectDetailCaptionWrap').css({ 'opacity': 1 });
                var projectDetailCaption1 = new RevealFx(document.querySelector('#projectDetailCaption1'), {
                  revealSettings : {
                    bgcolor: '#fa5123',
                    onCover: function(contentEl, revealerEl) {
                      contentEl.style.opacity = 1;
                    }
                  }
                });
                projectDetailCaption1.reveal();

                var projectListCaption2 = new RevealFx(document.querySelector('#projectDetailCaption2'), {
                  revealSettings : {
                    bgcolor: '#ff9e14',
                    delay: 250,
                    onCover: function(contentEl, revealerEl) {
                      contentEl.style.opacity = 1;
                    }
                  }
                });
                projectListCaption2.reveal();

              }

              commonInit.animateBg(index, nextIndex);

            }

    });
  }
}

var initServiceDetailAnimations = {
  initFullpage: function() {
    $('#serviceDetailFullpage').fullpage({
      //Navigation
            //menu: '#menu',
            lockAnchors: false,
            navigation: false,
            navigationPosition: 'right',
            showActiveTooltip: false,
            slidesNavigation: false,
            slidesNavPosition: 'bottom',

            //Scrolling
            css3: false,
            scrollingSpeed: 600,
            autoScrolling: true,
            fitToSection: false,
            fitToSectionDelay: 1500,
            scrollBar: false,
            //easing: 'easeInOutCubic',
            //easingcss3: 'ease',
            loopBottom: false,
            loopTop: false,
            loopHorizontal: true,
            continuousVertical: false,
            continuousHorizontal: false,
            scrollHorizontally: false,
            interlockedSlides: false,
            dragAndMove: false,
            offsetSections: false,
            resetSliders: false,
            fadingEffect: false,
            // normalScrollElements: '#normalScroll',
            scrollOverflow: true,
            scrollOverflowReset: false,
            scrollOverflowOptions: {
                scrollbars: false,
                mouseWheel: true,
                hideScrollbars: false,
                fadeScrollbars: false,
                disableMouse: true,
                interactiveScrollbars: true
            },
            touchSensitivity: 15,
            bigSectionsDestination: null,

            //Accessibility
            keyboardScrolling: true,
            animateAnchor: true,
            recordHistory: true,

            //Design
            controlArrows: true,
            verticalCentered: true,
            //sectionsColor: ['#ccc', '#fff'],
            paddingTop: '0px',
            paddingBottom: '0px',
            //fixedElements: '#header, .footer',
            responsiveWidth: 0,
            responsiveHeight: 0,
            responsiveSlides: false,
            parallax: false,
            parallaxOptions: { type: 'reveal', percentage: 62, property: 'translate' },
            cards: false,
            cardsOptions: { perspective: 100, fadeContent: true, fadeBackground: true },

            //Custom selectors
            sectionSelector: '.slide-section',
            lazyLoading: true,

            onLeave: function(index, nextIndex, direction) {

              commonInit.hideComponents();
              commonInit.endContactAnimate();
              
              initProjectListAnimations.animateProjectsList(nextIndex, 3, 1);

              var $leftCol = $('#serviceDetailFullpage .leftCol');
              var $rightCol = $('#serviceDetailFullpage .rightCol');
              $leftCol.removeClass('animations fadeInUp');
              $rightCol.removeClass('animations fadeInUp');
              if(nextIndex == 2) {
                $leftCol.addClass('animations fadeInUp').css('animation-delay', '0.6s');
                $rightCol.addClass('animations fadeInUp').css('animation-delay', '0.8s');
              }

              if(nextIndex == 4) {
                commonInit.startContactAnimate();
              }

            },
            afterLoad: function(index, nextIndex) {
              commonInit.showComponents();
              
              var $serviceDesc = $('#serviceDetailFullpage .description');
              var $serviceLogo = $('#serviceDetailFullpage .serviceLogo');
              
              $serviceDesc.removeClass('animations fadeInUp');
              $serviceLogo.removeClass('animations zoomIn');
              
              $('#serviceListCaptionWrap').css({ 'opacity': 0 });

              if( nextIndex == 1 ) {
                
                $('#serviceListCaptionWrap').css({ 'opacity': 1 });
                var serviceListCaption1 = new RevealFx(document.querySelector('#serviceListCaption1'), {
                  revealSettings : {
                    bgcolor: '#f6323e',
                    onCover: function(contentEl, revealerEl) {
                      contentEl.style.opacity = 1;
                    }
                  }
                });

                var serviceListCaption2 = new RevealFx(document.querySelector('#serviceListCaption2'), {
                  revealSettings : {
                    bgcolor: '#ff9e14',
                    delay: 250,
                    onCover: function(contentEl, revealerEl) {
                      contentEl.style.opacity = 1;
                    }
                  }
                });

                serviceListCaption1.reveal();
                serviceListCaption2.reveal();
                $serviceDesc.addClass('animations fadeInUp').css('animation-delay', '0.8s');
                $serviceLogo.addClass('animations zoomIn').css('animation-delay', '0.6s');

              }

              commonInit.animateBg(index, nextIndex);

            }

    });
  }
}

window.addEventListener('load', AOS.refresh);
$(window).on('load', function() {
  AOS.refresh();
});

$(window).load(function() {
  setTimeout(() => {
    initHomepageAnimations.initFullpage();
    initProjectListAnimations.initFullpage();
    initProjectDetailAnimations.initFullpage();
    initServiceDetailAnimations.initFullpage();
  }, 1000);
  commonInit.initTilt();
  commonInit.initCursor();
  AOS.init();
});